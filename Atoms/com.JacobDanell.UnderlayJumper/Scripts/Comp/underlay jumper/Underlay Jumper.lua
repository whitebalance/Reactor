--[[

A script that shows you all your underlays and let you jump to them quickly!

Colors (BRG:
	Orange = {0, 0.92156862745098, 0.43137254901961},

	Apricot = {0.2, 1, 0.65882352941176},
	
	yellow = {0.10980392156863, 0.88627450980392, 0.66274509803922},

	Lime = {0.082352941176471, 0.62352941176471, 0.77647058823529},
	
	Olive = {0.12549019607843, 0.37254901960784, 0.6},
	
	Green = {0.39607843137255, 0.25098039215686, 0.56078431372549},
	
	Teal = {0.6, 0, 0.59607843137255},
	
	Navy = {0.51764705882353, 0.082352941176471, 0.3843137254902},
	
	Blue = {0.8156862745098, 0.47450980392157, 0.65882352941176},
	
	Purple = {0.62745098039216, 0.6, 0.45098039215686},
	
	Violet = {0.80392156862745, 0.5843137254902, 0.29411764705882},
	
	Pink = {0.70980392156863, 0.91372549019608, 0.54901960784314},
	
	Tan = {0.5921568627451, 0.72549019607843, 0.69019607843137},
	
	Beige = {0.46666666666667, 0.77647058823529, 0.62745098039216},
	
	Brown = {0, 0.6, 0.4},
	
	Chocolate = {0.24705882352941, 0.54901960784314, 0.35294117647059},

]]

local scriptName = "underlay jumper"

local flow = comp.CurrentFrame.FlowView

local ui = app.UIManager
local disp = bmd.UIDispatcher(ui)


if #comp:GetToolList(false, "Underlay") == 0 then
	print("No underlays found")
	return
end

-- Map ESC to close UI
app:AddConfig("MyWin", {
    Target {ID = "MyWin"}, Hotkeys {
        Target = "MyWin",
        Defaults = true,
        ESCAPE = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {})]]}",
        UP = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {move = 'up'})]]}",
        DOWN = "Execute{cmd = [[app.UIManager:QueueEvent(obj, 'Close', {move = 'down'})]]}",
    }
})

local colorList = { -- BRG
	{0, 0.92156862745098, 0.43137254901961, "Orange"},
	{0.2, 1, 0.65882352941176, "Apricot"},
	{0.10980392156863, 0.88627450980392, 0.66274509803922, "Yellow"},
	{0.082352941176471, 0.62352941176471, 0.77647058823529, "Lime"},
	{0.12549019607843, 0.37254901960784, 0.6, "Olive"},
	{0.39607843137255, 0.25098039215686, 0.56078431372549, "Green"},
	{0.6, 0, 0.59607843137255, "Teal"},
	{0.51764705882353, 0.082352941176471, 0.3843137254902, "Navy"},
	{0.8156862745098, 0.47450980392157, 0.65882352941176, "Blue"},
	{0.62745098039216, 0.6, 0.45098039215686, "Purple"},
	{0.80392156862745, 0.5843137254902, 0.29411764705882, "Violet"},
	{0.70980392156863, 0.91372549019608, 0.54901960784314, "Pink"},
	{0.5921568627451, 0.72549019607843, 0.69019607843137, "Tan"},
	{0.46666666666667, 0.77647058823529, 0.62745098039216, "Beige"},
	{0, 0.6, 0.4, "Brown"},
	{0.24705882352941, 0.54901960784314, 0.35294117647059, "Chocolate"},
}

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end


win = disp:AddWindow({
	ID = 'MyWin',
    TargetID = 'MyWin',
	WindowTitle = 'Underlay Jumper',
	Spacing = 0,

	ui:VGroup{
		ID = 'root',
		ui.LineEdit{
            ID = 'SearchText',
            Weight = 0.0,
			Events = {
            	ReturnPressed = true,
            	TextChanged = true,
			},
        },
		ui:Tree{
			Alignment = { AlignHCenter = true, AlignTop = true, },
			ID = 'Tree',
			Events = {
				ItemDoubleClicked=true,
			},
		},
	},
})

function win.On.MyWin.Test(ev)
end

-- Add your GUI element based event functions here:
local itm = win:GetItems()
-- Resize UI to nice size
itm.MyWin.UpdatesEnabled = false
itm.MyWin:Resize( {430, 430} )
itm.MyWin.UpdatesEnabled = true

local tree = itm.Tree
local curTreeIndex = 1

-- Add a header row.
local newTreeItem = tree:NewItem()
-- As I can't seem to find a way to find all tree items, add a first item that is searchable
newTreeItem.Text[0] = 'Data'
newTreeItem.Text[1] = 'Name'
newTreeItem.Text[2] = 'Color'
newTreeItem.Text[3] = 'Comment'
tree:SetHeaderItem(newTreeItem)

-- Number of columns in the Tree list
tree.ColumnCount = 4

-- Resize the Columns
tree.ColumnWidth[0] = 0
tree.ColumnWidth[1] = 200
tree.ColumnWidth[2] = 100
tree.ColumnWidth[3] = 100

tree:SortByColumn(1, "AscendingOrder")

local Underlays = {}

function updateUnderlays()
	local allUnderlays = comp:GetToolList(false, "Underlay")

	-- Add an new row entries to the list
	for i,v in ipairs(allUnderlays) do

		local colorName = ""
		
		if v.TileColor == nil then -- if no color is set, standard is Tan	
			colorName = "Tan"
		else
			for i,c in ipairs(colorList) do
				if round(v.TileColor.B, 2) == round(c[1], 2) then
					if round(v.TileColor.R, 2) == round(c[2], 2) then
						if round(v.TileColor.G, 2) == round(c[3], 2) then
							colorName = c[4]
						end
					end
				end
			end
		end

		local underlay = {}
		underlay.Name = v.Name
		underlay.Color = colorName
		underlay.Comments = v.Comments[1]

		table.insert(Underlays, underlay)
	end
end
updateUnderlays()

function filterTree()
	tree.UpdatesEnabled = false
	tree.SortingEnabled = false

	local key = itm.SearchText.Text
	g_FilterCount = 0

	for i,v in ipairs(Underlays) do
		if v._TreeItem then
			local hide = true

			if #key == 0 or v.Name:lower():match(key:lower()) then
				hide = false
			end

			if hide ~= v.Hidden then
				v.Hidden = hide
				v._TreeItem.Hidden = hide
			end
		end
	end

	tree.UpdatesEnabled = true
	tree.SortingEnabled = true

	-- If no selected treeitem is any longer selected, select the first one 
	local treeItems = tree:FindItems("searchable")
	local found = false

	-- Find the first not hidden treeitem
	for i,treeItem in pairs(treeItems) do
		if not treeItem.Hidden then
			firstNotHiddenIndex = i
			break
		end
	end

	-- Select the first not hidden treeitem
	treeItems[curTreeIndex]:SetSelected(false)
	curTreeIndex = firstNotHiddenIndex
	treeItems[curTreeIndex]:SetSelected(true)
	
end

function populateTree()

	tree.UpdatesEnabled = false
	tree.SortingEnabled = false

	tree:Clear()

	for i,v in ipairs(Underlays) do

		local it = tree:NewItem()
		it.Text[0] = "searchable"
		it.Text[1] = v.Name
		it.Text[2] = v.Color
		it.Text[3] = v.Comments[1]

		if v.Color ~= "" then
			it.Icon[1] = ui:Icon{File = 'Scripts:/Comp/' .. scriptName .. '/img/' .. v.Color .. '.png'}
		end

		tree:AddTopLevelItem(it)

		v._TreeItem = it
		v._Hidden = false
	end

	tree.SortingEnabled = true
	tree.UpdatesEnabled = true

	filterTree();

end
populateTree()


function selectUnderlay(toolName)
	target = comp:FindTool(toolName)
	comp.CurrentFrame.FlowView:Select()
	comp:SetActiveTool(target)
end

-- A Tree view row was double clicked on
function win.On.Tree.ItemDoubleClicked(ev)
	selectUnderlay(tostring(ev.item.Text[0]))
end

function win.On.SearchText.TextChanged(ev)
	filterTree()
end

function win.On.SearchText.ReturnPressed(ev)
	local treeItems = tree:FindItems("searchable")
	if #treeItems > 0 then
		for i, treeItem in pairs(treeItems) do
			if treeItem:GetSelected() then
				selectUnderlay(treeItem:GetText(1))
				break
			end
		end
	end
	disp:ExitLoop()
end

function selectTree(amount, min, max)
	local treeItems = tree:FindItems("searchable")
	treeItems[curTreeIndex]:SetSelected(false)

	local found = false

	while not found do
		curTreeIndex = curTreeIndex + amount

		if curTreeIndex == min then
			curTreeIndex = max
		end

		if not treeItems[curTreeIndex].Hidden then
			found = true
		end
	end

	treeItems[curTreeIndex]:SetSelected(true)
end

-- The window was closed
function win.On.MyWin.Close(ev)
	if ev.move == nil then
		disp:ExitLoop()
	else
		local treeItems = tree:FindItems("searchable")
		if #treeItems > 0 then
			if ev.move == "up" then
				selectTree(-1, 0, #treeItems)
			else -- down
				selectTree(1, #treeItems + 1, 1)
			end
		end
	end
end

win:Show()
disp:RunLoop()
win:Hide()
