--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_SpotRemover = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Comments = Input { Value = "Macro by Emilio Sapia - Millolab\nhttps://emiliosapia.myportfolio.com", },
				Input = InstanceInput {
					SourceOp = "_IN_",
					Source = "Input",
				},
				Depth = InstanceInput {
					SourceOp = "_IN_",
					Source = "Depth",
					Default = 0,
				},
				Filter = InstanceInput {
					SourceOp = "TO",
					Source = "Filter",
				},
				Passes = InstanceInput {
					SourceOp = "TO",
					Source = "Passes",
					Default = 4,
				},
				Input01 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank4",
				},
				Source = InstanceInput {
					SourceOp = "FROM",
					Source = "Center",
					Name = "Source",
					Page = "Controls",
				},
				Destination = InstanceInput {
					SourceOp = "TO",
					Source = "Center",
					Name = "Destination",
				},
				Input02 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank4",
				},
				SoftEdge = InstanceInput {
					SourceOp = "TO",
					Source = "SoftEdge",
					MaxScale = 0.03,
				},
				Width = InstanceInput {
					SourceOp = "TO",
					Source = "Width",
					Default = 0.06,
				},
				Height = InstanceInput {
					SourceOp = "TO",
					Source = "Height",
					Default = 0.06,
				},
				Input03 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank4",
				},
				Angle = InstanceInput {
					SourceOp = "TO",
					Source = "Angle",
					Default = 0,
				},
				OffsetAngle = InstanceInput {
					SourceOp = "TO",
					Source = "AngleOffset",
					Name = "Offset Angle",
					MaxScale = 90,
					Page = "Controls",
					Default = 0,
				},
				Size = InstanceInput {
					SourceOp = "Transform1",
					Source = "TEST",
					Name = "Size",
					Page = "Controls",
					Default = 1,
				},
				Input04 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank4",
				},
				Softness = InstanceInput {
					SourceOp = "COLORS",
					Source = "XBlurSize",
					Name = "Softness",
					Default = 20,
				},
				Details = InstanceInput {
					SourceOp = "DETAILS",
					Source = "XBlurSize",
					Name = "Details",
					Default = 7,
				},
				Mix = InstanceInput {
					SourceOp = "MIX",
					Source = "Blend",
					Name = "Details Mix",
					Default = 0.5,
				},
				Input05 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank4",
				},
				Blend = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blend",
					Default = 1,
				},
				PaintOnly = InstanceInput {
					SourceOp = "Clear",
					Source = "Blend",
					Name = "Paint Only",
					Page = "Controls",
					Default = 0,
				},
				Input3 = InstanceInput {
					SourceOp = "Merge2",
					Source = "ProcessWhenBlendIs00",
					Page = "Common",
					Default = 0,
				},
				Input4 = InstanceInput {
					SourceOp = "Merge2",
					Source = "ProcessRed",
					Name = "Process",
					ControlGroup = 4,
					Default = 1,
				},
				Input5 = InstanceInput {
					SourceOp = "Merge2",
					Source = "ProcessGreen",
					ControlGroup = 4,
					Default = 1,
				},
				Input6 = InstanceInput {
					SourceOp = "Merge2",
					Source = "ProcessBlue",
					ControlGroup = 4,
					Default = 1,
				},
				Input7 = InstanceInput {
					SourceOp = "Merge2",
					Source = "ProcessAlpha",
					ControlGroup = 4,
					Default = 1,
				},
				Input8 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank1",
				},
				Input9 = InstanceInput {
					SourceOp = "Merge2",
					Source = "ApplyMaskInverted",
					Default = 0,
				},
				Input10 = InstanceInput {
					SourceOp = "Merge2",
					Source = "MultiplyByMask",
					Default = 0,
				},
				Input11 = InstanceInput {
					SourceOp = "Merge2",
					Source = "FitMask",
				},
				Input12 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank2",
				},
				Input13 = InstanceInput {
					SourceOp = "Merge2",
					Source = "MaskChannel",
					Default = 3,
				},
				Input14 = InstanceInput {
					SourceOp = "Merge2",
					Source = "MaskLow",
					ControlGroup = 11,
					Default = 0,
				},
				Input15 = InstanceInput {
					SourceOp = "Merge2",
					Source = "MaskHigh",
					ControlGroup = 11,
					Default = 1,
				},
				Input16 = InstanceInput {
					SourceOp = "Merge2",
					Source = "MaskClipBlack",
					Name = "Black",
					Default = 1,
				},
				Input17 = InstanceInput {
					SourceOp = "Merge2",
					Source = "MaskClipWhite",
					Name = "White",
					Default = 1,
				},
				Input18 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank4",
				},
				EffectMask = InstanceInput {
					SourceOp = "Merge2",
					Source = "EffectMask",
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "AutoDomain1_1",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo { Pos = { 750, 408.576 } },
			Tools = ordered() {
				DETAILS = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						XBlurSize = Input { Value = 7, },
						Input = Input {
							SourceOp = "_IN_",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -189.8, 62.6978 } },
				},
				ChannelBooleans1 = ChannelBoolean {
					CtrlWShown = false,
					Inputs = {
						Operation = Input { Value = 2, },
						ToAlpha = Input { Value = 4, },
						Background = Input {
							SourceOp = "_IN_",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "DETAILS",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -60.7999, 62.6978 } },
				},
				_IN_ = ChangeDepth {
					CtrlWZoom = false,
					CtrlWShown = false,
					NameSet = true,
					ViewInfo = OperatorInfo { Pos = { -376.4, 278.29 } },
				},
				TO = EllipseMask {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						PaintMode = Input { Value = FuID { "Multiply" }, },
						OutputSize = Input { Value = FuID { "Custom" }, },
						MaskWidth = Input {
							Value = 2702,
							Expression = "_IN_.Input.OriginalWidth",
						},
						MaskHeight = Input {
							Value = 1520,
							Expression = "_IN_.Input.OriginalHeight",
						},
						PixelAspect = Input { Value = { 1, 1 }, },
						ClippingMode = Input { Value = FuID { "None" }, },
						Center = Input { Value = { 0.4, 0.5 }, },
						Width = Input { Value = 0.06, },
						Height = Input { Value = 0.06, },
					},
					ViewInfo = OperatorInfo { Pos = { -98.38, 139.99 } },
					UserControls = ordered() {
						AngleOffset = {
							INP_Integer = false,
							LINKID_DataType = "Number",
							INP_Default = 0,
							ICS_ControlPage = "Controls",
							INPID_InputControl = "ScrewControl",
							LINKS_Name = "AngleOffset"
						}
					}
				},
				FROM = EllipseMask {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "TO",
							Source = "Mask",
						},
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "TO.Filter",
						},
						SoftEdge = Input { Expression = "TO.SoftEdge", },
						PaintMode = Input { Value = FuID { "Ignore" }, },
						OutputSize = Input { Value = FuID { "Custom" }, },
						MaskWidth = Input { Value = 4096, },
						MaskHeight = Input { Value = 1806, },
						PixelAspect = Input { Value = { 1, 1 }, },
						Width = Input {
							Value = 0.06,
							Expression = "TO.Width",
						},
						Height = Input {
							Value = 0.06,
							Expression = "TO.Height",
						},
						Angle = Input { Expression = "TO.Angle+TO.AngleOffset", },
						Center = Input { Value = { 0.6, 0.5 }, }
					},
					ViewInfo = OperatorInfo { Pos = { 10.9899, 139.99 } },
					UserControls = ordered() {
						Center = {
							INPID_PreviewControl = "CrosshairControl",
							LINKID_DataType = "Point",
							ICS_ControlPage = "Controls",
							INPID_InputControl = "OffsetControl",
							CHC_Style = "DiagonalCross",
							LINKS_Name = "Center"
						}
					}
				},
				COLORS = Blur {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "FROM",
							Source = "Mask",
						},
						MultiplyByMask = Input { Value = 1, },
						Filter = Input { Value = FuID { "Fast Gaussian" }, },
						XBlurSize = Input { Value = 20, },
						Input = Input {
							SourceOp = "_IN_",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 162.82, 200.45 } },
				},
				MIX = Custom {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Blend = Input { Value = 0.5, },
						LUTIn1 = Input {
							SourceOp = "MIXLUTIn1",
							Source = "Value",
						},
						LUTIn2 = Input {
							SourceOp = "MIXLUTIn2",
							Source = "Value",
						},
						LUTIn3 = Input {
							SourceOp = "MIXLUTIn3",
							Source = "Value",
						},
						LUTIn4 = Input {
							SourceOp = "MIXLUTIn4",
							Source = "Value",
						},
						RedExpression = Input { Value = "c1+2*c2-1", },
						GreenExpression = Input { Value = "c1+2*c2-1", },
						BlueExpression = Input { Value = "c1+2*c2-1", },
						Image1 = Input {
							SourceOp = "COLORS",
							Source = "Output",
						},
						Image2 = Input {
							SourceOp = "AutoDomain1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 376, 200.45 } },
				},
				MIXLUTIn4 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 204, Green = 204, Blue = 204 },
				},
				MIXLUTIn3 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 0, Blue = 204 },
				},
				MIXLUTIn2 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 0, Green = 204, Blue = 0 },
				},
				MIXLUTIn1 = LUTBezier {
					KeyColorSplines = {
						[0] = {
							[0] = { 0, RH = { 0.333333333333333, 0.333333333333333 }, Flags = { Linear = true } },
							[1] = { 1, LH = { 0.666666666666667, 0.666666666666667 }, Flags = { Linear = true } }
						}
					},
					SplineColor = { Red = 204, Green = 0, Blue = 0 },
				},
				Clear = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Alpha = Input { Value = 1, },
						Gain = Input { Value = 0, },
						Input = Input {
							SourceOp = "_IN_",
							Source = "Output",
						},
						Blend = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 159.76, 278.29 } },
					UserControls = ordered() {
						Blend = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "CheckboxControl",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							CBC_TriState = false,
							LINKS_Name = "Blend"
						}
					}
				},
				Merge2 = Merge {
					CtrlWShown = false,
					Inputs = {
						Background = Input {
							SourceOp = "Clear",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "MIX",
							Source = "Output",
						},
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 376, 278.29 } },
				},
				AutoDomain1_1 = AutoDomain {
					CtrlWShown = false,
					Inputs = {
						Input = Input {
							SourceOp = "Merge2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 376, 334.8 } },
				},
				Merge1 = Merge {
					CtrlWShown = false,
					Inputs = {
						EffectMask = Input {
							SourceOp = "FROM",
							Source = "Mask",
						},
						MultiplyByMask = Input { Value = 1, },
						Background = Input {
							SourceOp = "ChannelBooleans1",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Transform1",
							Source = "Output",
						},
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 122.78, 62.6978 } },
				},
				BrightnessContrast1 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						Brightness = Input { Value = 0.5, },
						Input = Input {
							SourceOp = "Merge1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 244.02, 62.6978 } },
				},
				AutoDomain1 = AutoDomain {
					CtrlWShown = false,
					Inputs = {
						Input = Input {
							SourceOp = "BrightnessContrast1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 376, 62.6978 } },
				},
				Transform1 = Transform {
					CtrlWShown = false,
					Inputs = {
						Center = Input {
							Value = { 0.508360797570204, 0.422828923602219 },
							Expression = "Point(0.5+(TO.Center.X-FROM.Center.X), 0.5+(TO.Center.Y-FROM.Center.Y))",
						},
						Pivot = Input {
							Value = { 0.551882341993481, 0.513800306926195 },
							Expression = "FROM.Center",
						},
						Size = Input { Expression = "TEST", },
						Angle = Input { Expression = "FROM.Angle", },
						Input = Input {
							SourceOp = "ChannelBooleans1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 122.78, 8.71466 } },
					UserControls = ordered() {
						TEST = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 5,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICS_ControlPage = "Controls",
							ICD_Center = 1,
							LINKS_Name = "TEST"
						}
					}
				}
			},
		}
	},
	ActiveTool = "ml_SpotRemover"
}