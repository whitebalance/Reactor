--[[--
Rainier Mood

This fuse creates the impression of rain on a water surface, which is placed over the footage.
The following parameters can be set.

1) Center: moves the center of the image to the footage
2) Scale: adjust the size of the drops
3) Angle: turns the carpet of droplets
4) Waveheight: changes the visibility of the drops
5) Frequency: the rate of change of the drops
6) Clump: pulls the carpet into a lump
7) Damping: Changes the distribution of the drops
8) Constrict X / Y: the carpet of drops can be stretched or pushed together horizontally (x) / vertically (y)
9) Hash Values: the hash values for the random changes can be adjusted.

I would like to take this opportunity to thank Shem Namo (LearnNowFX) again for his support and help.


Based on https://www.shadertoy.com/view/ldfyzl


V0.1 Initial - without any paramter - the original transpose of Shadertoy
V0.2 Center, size, waveheight, clump, damp, freq, hsh22 added
V0.3 Reconstruction after the corrections by Shem Namo
  
--]] --

--------------------------------
-- Registry declaration

FuRegisterClass(
    "RainierMood",
    CT_Tool,
    {
        REGS_Category = "Fuses",
        REGS_OpIconString = "RaM",
        REGS_OpDescription = "RainierMood",
        REG_Fuse_NoEdit = true,
        REG_Fuse_NoReload = true,
        REG_TimeVariant = true,
        REGS_Company = "JiPi",
        REGS_URL = "https://www.steakunderwater.com/wesuckless/",
        REG_Version = 000003
    }
)

version = "Rainier Mood V0.3 - 6 Okt 2020"
--------------------------------
-- Description of kernel parameters

RainierMoodParams =
    [[
  float debug;
  float scale;
  float waveheight;
  float clump;
  float damp;
  float freq;
  float phase;
  float hash22[3];
  float constrict[2];
  float center[2];
  float angle;
  int srcsize[2];
  float itime;
  int compOrder;
]]

--------------------------------
-- source of kernel

RainierMoodKernel =
    [[
#define fract(a) a - _floor(a)  

// Maximum number of cells a ripple can cross.
#define MAX_RADIUS 2

// Set to 1 to hash twice. Slower, but less patterns.
#define DOUBLE_HASH 1
#define HASHSCALE1 .1031f

__DEVICE__ float hash12(float2 p)
{
  float3 p3  = to_float3(fract((float)p.x * HASHSCALE1) ,fract((float)p.y * HASHSCALE1) ,fract((float)p.x * HASHSCALE1) );
  p3 += dot(p3, p3 + 19.19f);    //xyz !!!!!
  return fract((p3.x + p3.y) * p3.z);
}

__DEVICE__ float2 hash22(float2 p, float3 HASHSCALE3)
{
  float3 p3  = to_float3(fract((float)p.x * HASHSCALE3.x) ,fract((float)p.y * HASHSCALE3.y) ,fract((float)p.x * HASHSCALE3.z) );
  p3 += dot(p3, p3+19.19f);  //xyz
  return to_float2(fract((p3.x+p3.y)*p3.z), fract((p3.x+p3.z)*p3.y) );  //xx, yz, zy
}

__DEVICE__ float2 spin(float2 uv, float angle)
{ 
   float C = _cosf(angle);
   float S = _sinf(angle);
   
   float x = uv.x;
   uv.x = (x * C - uv.y * S);
   uv.y = (x * S + uv.y * C);
   return uv;
}   


__KERNEL__ void RainierMoodKernel(__CONSTANTREF__ RainierMoodParams *params, __TEXTURE2D__ iChannel0, __TEXTURE2D_WRITE__ dst)
{
  DEFINE_KERNEL_ITERATORS_XY(x, y);
  //---------------------------------------
  if (x < params->srcsize[0] && y < params->srcsize[1]) 
  {
  
    float3 HASHSCALE3 = to_float3_v(params->hash22);
    
    float2 fragCoord = to_float2(x, y); 
    float2 iResolution = to_float2(params->srcsize[0] - 1, params->srcsize[1] - 1);
  
    float resolution = 10.0f * params->scale;
    float2 uv = fragCoord / iResolution.y * resolution;
    float2 uuv = uv; // + params->center;
    uuv.x -= ((params->center[0]-0.5f) * 10.17f*(iResolution.x/iResolution.y))*params->scale;
    uuv.y -= ((params->center[1]-0.5f) * 5.72f*(iResolution.x/iResolution.y))*params->scale; //1.777;

    float2 cor = 5.0f*to_float2(params->scale*1.77f,params->scale);
    uuv = spin(uuv-cor, params->angle);
    
    float2 p0 = _floor(uuv);
    float2 circles = to_float2_s(0.0f);
  
    for (int j = -MAX_RADIUS; j <= MAX_RADIUS; ++j)
      {
      for (int i = -MAX_RADIUS; i <= MAX_RADIUS; ++i)
        {
        float2 pi = (p0 + to_float2(i, j))/params->clump;
        #if DOUBLE_HASH
          float2 hsh = hash22(pi, HASHSCALE3);
        #else
          float2 hsh = pi;
        #endif
        
        pi.x = pi.x/params->constrict[0];
        pi.y = pi.y/params->constrict[1];
        
        float2 p = pi + hash22(hsh*params->damp, HASHSCALE3)*params->damp;
          
        float t = fract(params->freq*params->itime + hash12(hsh));
        float2 v = p - uuv;
        float d = length(v) - ((float)MAX_RADIUS + 1.0f)*t;
  
        float h = 1e-3;
        float d1 = d - h;
        float d2 = d + h;
        float p1 = _sinf(31.0f*d1) * smoothstep(-0.6f, -0.3f, d1) * smoothstep(0.0f, -0.3f, d1);
        float p2 = _sinf(31.0f*d2) * smoothstep(-0.6f, -0.3f, d2) * smoothstep(0.0f, -0.3f, d2);
        circles += 0.5f * normalize(v) * ((p2 - p1) / (2.0f * h) * (1.0f - t) * (1.0f - t));
        }
      }
    circles /= (float)(MAX_RADIUS*params->waveheight+1)*(MAX_RADIUS*params->waveheight+1);
    float intensity = _mix(0.01f, 0.15f, smoothstep(0.1f, 0.6f, _fabs(fract(0.05f*params->itime + 0.5f)*2.0f-1.0f)));
    
    float3 n = to_float3_aw(circles, _sqrtf(1.0f - dot(circles, circles)));
    float3 color =  to_float3_s(5.0f*_powf(_clampf(dot(n, normalize(to_float3(1.0f, 0.7f, 0.5f))), 0.0f, 1.0f), 6.0f)); //!!!!!
    float4 fragColor = to_float4_aw(color, 1.0);
    
    float4 image = _tex2DVecN( iChannel0, (uv.x/resolution - intensity*n.x)/(iResolution.x/iResolution.y), (uv.y/resolution - intensity*n.y),15);
    fragColor += image; 
    
    _tex2DVec4Write(dst, x, y, fragColor);
  }
}
]]

function Create()
  InCenter = self:AddInput("Center", "Center", {
    LINKID_DataType = "Point",
    INPID_InputControl = "OffsetControl",
    INPID_PreviewControl = "CrosshairControl",
    })

  InScale = self:AddInput("Scale", "Scale", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    RCD_LockAspect     = 1,
    RC_DoLockAspect    = true,
    RCID_Center        = "Center", 
    INP_Default = 1.0,
    INP_MinAllowed = 0.1,
    INP_MaxAllowed = 5.0,
    })

  InDebug = self:AddInput("Debug", "Debug", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 1.0,
    --INP_MinScale = 0.0,
    --INP_MaxScale = 5.0,
    --INPID_PreviewControl = "RectangleControl",
    RCD_LockAspect     = 1,
    RC_DoLockAspect    = true,
    RCID_Center = "Center",    
    IC_Visible         = false, 
  })

  InAngle = self:AddInput("Angle", "Angle", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ScrewControl",
    INPID_PreviewControl = "AngleControl",
    INP_Default = 0.0,
    ACID_Center = "Center",
    --PC_GrabPriority = 1 -- give this a higher priority than the rectangle
    })    

  InWaveHeight = self:AddInput("Wave Height", "WaveHeight", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 2.0,
    INP_MinAllowed = 0.6,
    INP_MaxAllowed = 4.0,
    })

  InDamping = self:AddInput("Damping", "Damping", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 1.0,
    INP_MinAllowed = 0.0,
    INP_MaxAllowed = 100.0,
    })

  InFrequency = self:AddInput("Frequency", "Frequency", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 0.3,
    INP_MinAllowed = -1.0,
    INP_MaxAllowed = 1.0,
    })

  InPhase = self:AddInput("Phase", "Phase", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ScrewControl",
    INP_Default = 0.0,
    INP_MaxScale = 10.0,
    IC_Visible  = false,
    })

  InClump = self:AddInput("Clump", "Clump", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ScrewControl",
    INP_Default = 1.0,
    INP_MinAllowed = 1.0,
    INP_MaxAllowed = 30.0,
    })
  InDamping = self:AddInput("Damping", "Damping", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 1.0,
    INP_MinAllowed = 0.0,
    INP_MaxAllowed = 100.0,
    })
  
  InConstrictX = self:AddInput("Constrict X", "ConstrictX", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 1.0,
    INP_MinAllowed = 0.1,
    INP_MaxAllowed = 20.0,
    })
  InConstrictY = self:AddInput("Constrict Y", "ConstrictY", {
    LINKID_DataType = "Number",
    INPID_InputControl = "SliderControl",
    INP_Default = 1.0,
    INP_MinAllowed = 0.1,
    INP_MaxAllowed = 20.0,
    })
  
  InConstrict = self:AddInput("Constrict","Constrict", {
    LINKID_DataType = "Point",
    INPID_InputControl = "OffsetControl",
    INP_DoNotifyChanged = false,
    INP_DefaultX = 1,
    INP_DefaultY = 1,
    XF_XAxis = -1,
    XF_YAxis = -1,
    XF_XSize = 1,
    XF_YSize = 1,
    XF_Angle = 0,
    XF_EdgeMode = "Black",
    IC_Visible  = false,
    })

self:BeginControlNest("Hash Values", "HashValues", false, {})
 InHash22X = self:AddInput("X", "X", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ScrewControl",
    INP_Default = 0.1031,
    INP_MaxScale = 10.0,
    })
 InHash22Y = self:AddInput("Y", "Y", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ScrewControl",
    INP_Default = 0.1030,
    INP_MaxScale = 10.0,
    })
  InHash22Z = self:AddInput("Z", "Z", {
    LINKID_DataType = "Number",
    INPID_InputControl = "ScrewControl",
    INP_Default = 0.0973,
    INP_MaxScale = 10.0,
    })
self:EndControlNest()

  InLabel = self:AddInput(version, "version", {
    LINKID_DataType = "Text",
    INPID_InputControl = "LabelControl",
    INP_External = false,
    INP_Passive = true,
    ICD_Width = 1,
  })

  -- In/Out --
  InImage = self:AddInput("Image", "Image", {
    LINKID_DataType = "Image",
    LINK_Main = 1,
    --INP_Required = false
  })
  OutImage = self:AddOutput("Output", "Output", {
    LINKID_DataType = "Image",
    LINK_Main = 1,
  })
end 
  

local lastreqtime = -2

--################################# PROCESS ######################################
function Process(req)
    local center = InCenter:GetValue(req)
    local angle = InAngle:GetValue(req).Value
    local debug = InDebug:GetValue(req).Value
    local scale = InScale:GetValue(req).Value
    local damp = InDamping:GetValue(req).Value
    local freq = InFrequency:GetValue(req).Value
    local phase = InPhase:GetValue(req).Value
    local waveheight = InWaveHeight:GetValue(req).Value
    local clump = InClump:GetValue(req).Value
    --local hash22[3] = {InHash22X:GetValue(req).Value, InHash22Y:GetValue(req).Value, InHash22Z:GetValue(req).Value}

    local src = InImage:GetValue(req)
    local dst = Image {IMG_Like = src, IMG_DeferAlloc = true}

    local node = DVIPComputeNode(req, "RainierMoodKernel", RainierMoodKernel, "RainierMoodParams", RainierMoodParams)

    if (lastreqtime ~= req.Time - 1) then -- Use, if CT_Tool
        params = node:GetParamBlock(RainierMoodParams)
    end
    lastreqtime = req.Time

    params.scale = scale
    params.debug = debug
    params.waveheight = waveheight
    params.clump = clump
    params.damp = damp
    params.freq = freq
    params.phase = phase
    params.hash22 = {InHash22X:GetValue(req).Value, InHash22Y:GetValue(req).Value, InHash22Z:GetValue(req).Value}
    --params.constrict[0] = InConstrict:GetValue(req).X
    --params.constrict[1] = InConstrict:GetValue(req).Y
    params.constrict[0] = InConstrictX:GetValue(req).Value
    params.constrict[1] = InConstrictY:GetValue(req).Value
    params.center[0] = center.X
    params.center[1] = center.Y
    params.angle = angle

    params.compOrder = 15
    params.srcsize[0] = src.DataWindow:Width()
    params.srcsize[1] = src.DataWindow:Height()

    local framerate = self.Comp:GetPrefs("Comp.FrameFormat.Rate") -- get the frame rate of the comp set in the preferences
    params.itime = req.Time / framerate

    node:SetParamBlock(params)

    node:AddSampler("RowSampler", TEX_FILTER_MODE_LINEAR, TEX_ADDRESS_MODE_CLAMP, TEX_NORMALIZED_COORDS_TRUE)

    node:AddInput("src", src)
    node:AddOutput("dst", dst)

    local success = node:RunSession(req)
    if not success then
        dst = nil
        dump(node:GetErrorLog()) -- Fehlerausgabe des DCTL
    end

    OutImage:Set(req, dst)
end
