--[[--
Show the metadata of the connected clip

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
----------------------------------------------------------------------

*	The fuse should be able to be faster. Clip with 30fps playback plays back at 20fps.
	*	If a connected nodetree has something that changes the timing like TimeStretch, stuffs breaks when fuse tried to play that clip as the in and outpoint now is -10000000 to 10000000


Changelog:
version 1.0, 2018-09-18: Initial release

--]]--

local gotData = false
local reqq

FuRegisterClass("ShowMetadata", CT_Tool, {
	REGS_Name = "Show Metadata",
	REGS_Category = "Metadata",
	REGS_OpIconString = "smd",
	REGS_OpDescription = "Show current metadata",
	
	REGS_Company = "Jacob Danell",
	REGS_URL = "http://www.emberlight.se",
	REG_Version = 100,

	REG_OpNoMask = true,
	REG_NoBlendCtrls = true,
	REG_NoObjMatCtrls = true,
	REG_NoMotionBlurCtrls = true,
})

function Create()
	InMode = self:AddInput("Mode", "Mode", {
		LINKID_DataType = "Number",
		INPID_InputControl = "MultiButtonControl",
		{ MBTNC_AddButton = "Plain text", },
		{ MBTNC_AddButton = "Induvidual controls", },
		MBTNC_StretchToFit = true,
		INP_Default = 0,
		INP_DoNotifyChanged = true,
		IC_Visible = false,
	})

	InMeta = self:AddInput("Metadata", "Metadata", {
		LINKID_DataType = "Text",
		INPID_InputControl = "TextEditControl",
		IC_ControlPage = 0,
		TEC_Lines = 20,
		TEC_Wrap = false,
		TEC_ReadOnly = true,
		ICD_Width = 1,
		Got_Data = 0,
	})		

	InImage = self:AddInput("Input", "Input", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
	})

	OutImage = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
	})				
end

function NotifyChanged(inp, param, time)
	if param then
		if inp == InMode then
		end
	end
end

function procesMetadata(req, img)
	local metadata = img.Metadata

	-- If plane text
	if InMode:GetValue(req).Value == 0 then
		local data = ""
		for k,v in pairs(metadata) do
			data = data .. k .. " = " .. v .. "\n"
		end
		InMeta:SetSource(Text(data), 0)
	else -- If induvidual controls

		--[[

		-- Delete all old controls first!
		local ctrl
		for i = 2, 64 do
			local ctrl = self:FindInput("ctrl"..(i))
			if ctrl ~= nil then
				ctrl
			end
		end

		local i = 0
		for k,v in pairs(metadata) do
			i = i + 1
			-- If number
			if tonumber(v) ~= nil then
				
				self:AddInput("ctrl"..(i), "ctrl"..(i), {
					LINKS_Name = k,
					LINKID_DataType = "Number",
					INPID_InputControl = "ScrewControl",
				})

			else -- If text

			end
		end

		]]--

	end
end

function OnConnected(inp, old, new)
	if inp == InImage and new ~= nil then
		-- Try to find a way to get metadata from here
    else
    	InMeta:SetSource(Text(""), 0)
	end
end

function Process(req)
	reqq = req
	local img = InImage:GetValue(req)

	-- Process data only once
	if gotData == 0 then
		gotData = 1
		procesMetadata(req, img)
	else
		gotData = 0
	end


	OutImage:Set(req, img)
end